import React from 'react';
import styled from 'styled-components';

export default ({
                  phrase,
                  separator = '__',
                  inverted = false
}) =>  {
  const DarkPart = styled.span`
    font-family: serif;
    color: #1e1e1e;
    font-weight: bolder;
    text-transform: uppercase;
    line-height: 45px;
  `;

  const LightPart = styled.span`
    color: #d8d8d8;
    font-weight: lighter;
    text-transform: uppercase;
    line-height: 45px;
  `;

  return (
    <h2>
      {phrase.split(separator).map((part, i) => {
        // FIXME looks a bit complicated and confusing
        const condition = inverted ? (i % 2) === 0 : (i % 2) !== 0;
        return condition ? <DarkPart key={part}>{part}</DarkPart> : <LightPart key={part}>{part}</LightPart>
      })}
    </h2>
  )
}