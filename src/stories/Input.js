//TODO add support for redux-form's Field enviroment

// import React from 'react';
//
// import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';
//
//
// import Input from '../components/Input';
//
// storiesOf('Input', module)
//   .add('empty text input', () => <Input onChange={ action('input change event')}/>)
//   .add('with custom (1s) throttling time', () => <Input throttlingTime={1000} onChange={ action('input change event')}/>)
//   .add('with custom (0.1s) throttling time', () => <Input throttlingTime={100} onChange={ action('input change event')}/>)
//   .add('empty text input with placeholder', () => <Input placeholder='Put your text here!' onChange={ action('input change event')}/>)
//   .add('purgeable input', () => <Input placeholder='Click X to clear' purgeable onChange={ action('input change event')} />)
//   .add('with numerical type', () => <Input type='number' placeholder='Only numbers and dashes are allowed' onChange={ action('input change event')} />)
//   .add('purgeable numerical type', () => <Input purgeable type='number' placeholder='Only numbers and dashes allowed' onChange={ action('input change event')} />)
