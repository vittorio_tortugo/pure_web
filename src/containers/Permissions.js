import React, {Component} from 'react';
import Screen from "../components/Screen";
import ZebraTitle from "../components/ZebraTitle";
import Button from "../components/Button";
import { permission } from "../services/permissions";
import { ROUTES } from "../constants/routes";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import img from '../images/hero.gif';
import styled from "styled-components";

const StyledScreen = styled(Screen)`
  img {
    max-width: 100%;
  }
`;

class Permissions extends Component {
  componentWillMount() {
    let { notifications } = this.props.permissions;

    if (notifications === 'granted') {
      this.props.next()
    }
  }

  requestPermission() {
    permission.requestNotification()
      .then( result => {
        if (result === 'denied') {
          console.log('Permission wasn\'t granted. Allow a retry.');
          return;
        }
        if (result === 'default') {
          console.log('The permission request was dismissed.');
          return;
        }

        this.props.addNotification();
        this.props.next()
      })
  }

  render() {
    return (
      <StyledScreen full>
        <ZebraTitle phrase={'Turn on notifications __ to get into hookups on time'} inverted />
        <div>
          <img src={img} alt="Be a hero"/>
        </div>
        <Button onClick={this.requestPermission.bind(this)}>Enable notifications</Button>
      </StyledScreen>
    )
  }
}

export default Permissions = connect(
  ({permissions}) => ({permissions}),
  dispatch => ({
    next() {
      dispatch(push(ROUTES.INDEX))
    },

    addNotification() {
      dispatch({
        type: 'ADDED_NOTIFICATION_PERMISSION'
      })
    }
  })
)(Permissions);