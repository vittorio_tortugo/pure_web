export const ROUTES = {
  INDEX: '',
  TERMS: 'terms',
  ENTER_PHONE: 'enter-phone',
  VERIFY_PHONE: 'verify-phone',
  PERMISSIONS: 'permissions'
};