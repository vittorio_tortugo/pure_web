import React from 'react';
import styled from 'styled-components';

const Error = styled.div`
  color: red;
`;

export default ({...props}) => (
  <Error {...props} />
)