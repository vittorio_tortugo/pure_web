import React, { Component } from 'react';
import Button from "../components/Button";
import soul from '../services/soul';
import { connect } from "react-redux";
import Screen from "../components/Screen";
import styled from "styled-components";
import { COLORS } from "../constants/theme";
import img from '../images/mask.png'
import { readFile } from "../services/filereader";

const StyledScreen = styled(Screen)`
  .photo {
    background: ${COLORS.DARK};
    padding: 40px 0;
    margin-bottom: 20px;
        
    img {
      max-width: 100%;
    }
  }
  
  button {
    width: 100%;
    pointer-events: none;
  }
  
  label {
    cursor: pointer;
    width: 100%;
  }
  
  input {
    display: none;
  }
  
  a {
    cursor: pointer;
  }
`;

class SendRequest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      img: img
    };
    this.loadCurrentPhoto();
  }

  loadCurrentPhoto() {
    soul.me.get()
      .then(res => {
        let photo = res.albums[0].mainPhoto.original.url;
        this.setState({img: photo})
      });
  }

  signout() {
    soul.auth.signOut();
    // FIXME just hack for signOut
    window.localStorage.removeItem('soulStorage:userId');

    window.location.reload();
  }

  upload(e) {
    soul.media.uploadPhoto({
      photo: e.target.files[0]
    }).then(res => {
      soul.media.setAlbumMainPhoto({
        albumName: 'default',
        photoId: res.id
      })
      .then(this.loadCurrentPhoto.bind(this))
    })

  }

  render() {
    const { phone } = this.props;
    let input;

    return (
      <StyledScreen full>
        <div className='photo'>
          <img src={this.state.img} alt="Your avatar"/>
        </div>

        <div>
          <p>Your account {phone.countryCode.code}{phone.number} <a onClick={this.signout.bind(this)}>(Sign out)</a></p>
          <label htmlFor="imgUploader"><Button>Add a photo</Button></label>
          <input ref={node => input = node} id='imgUploader' type="file" onChange={this.upload.bind(this)}/>
        </div>
      </StyledScreen>
    )
  }
}

export default SendRequest = connect(
  ({phone}) => ({phone})
)(SendRequest);