import React from 'react';

// import throttle from 'lodash/throttle';
// import { Icon } from 'react-fa';
import styled from 'styled-components';
import { Field } from 'redux-form';

// const Wrapper = styled.div`
//     display: inline-flex;
//     flex-direction: row;
//     align-items: center;
//     position: relative;
//   `;

const StyledInput = styled(Field)`
    padding: 8px 6px;
    font-size: 16px;
    outline: none;
    border: none;
    border-bottom: 1px solid #d8d8d8;
    color: #1e1e1e;
    -moz-appearance: textfield;
    ::-webkit-outer-spin-button,
    ::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
    ${({invalid}) => (invalid && 'color: red;')}
  `;

// const ResetIcon = styled(Icon)`
//     position: absolute;
//     right: 6px;
//     font-size: 18px;
//     color: #d8d8d8;
//     cursor: pointer;
//   `;

export default ({...props}) => (
  <StyledInput {...props} />
)
