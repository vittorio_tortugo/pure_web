import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import './Input';

import Button from '../components/Button';
import ZebraTitle from '../components/ZebraTitle';
import CountryCodeInput from '../components/Select';

import { Icon } from 'react-fa';
import ErrorMessage from "../components/ErrorMessage";

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('Click on the button')}>I'm down</Button>)
  .add('with text and icon', () => (
    <Button onClick={action('Click on the button')}>
      <Icon name='arrow-circle-right' />
      &nbsp;Click here
    </Button>))
  .add('disabled', () => <Button onClick={action('Click on the button')} disabled>I'm down</Button>)

storiesOf('ZebraTitle', module)
  .add('with few parts', () => <ZebraTitle phrase='Hello __ there __ i am __ a zebra text!' />)
  .add('started from another style', () => <ZebraTitle phrase='Hello __ there __ i am __ a zebra text!' inverted />);

storiesOf('Error', module)
  .add('with text', () => <ErrorMessage>Something went wrong</ErrorMessage>)