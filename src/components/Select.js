import React  from 'react';

import styled from 'styled-components';
import { Field } from "redux-form";

const StyledSelect = styled(Field)`
    padding: 8px 6px;
    font-size: 16px;
    outline: none;
    border-radius: 0px;
    border: none;
    border-bottom: 1px solid #d8d8d8;
    color: #1e1e1e;
    background: transparent;
    line-height: 1.5;
    background-color: #fff;
    background-image: none;
    background-clip: padding-box;
    -webkit-appearance: none;
    height: 40px;
  `;

export default ({...props}) => {
  return (
    <StyledSelect {...props} />
  )
}
