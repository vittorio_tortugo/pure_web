export const phoneReducer = (state = {}, action) => {
  switch(action.type) {
    case 'CHANGED_NUMBER':
      return Object.assign({}, state, {
        number: action.number,
        isVerified: false
      });

    case 'CHANGED_COUNTRY_CODE':
      return Object.assign({}, state, {
        countryCode: action.countryCode
      });

    case 'PHONE_VERIFIED':
      return Object.assign({}, state, {
        isVerified: true
      });

    default:
      return state;
  }
};

export const userReducer = (state = {}, action) => {
  switch(action.type) {
    case 'USER_AUTH':
      let { isAuthenticated } = action;
      state = Object.assign({}, state, {
        isAuthenticated
      });

      return state;

    default:
      return state;
  }
};

export const permissionReducer = (state = {}, action) => {
  switch(action.type) {
    case 'ADDED_NOTIFICATION_PERMISSION':

      return Object.assign({}, state, {
        notifications: 'granted'
      });

    default:
      return state;
  }
};