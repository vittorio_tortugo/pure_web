// TODO use http://thejameskyle.com/styled-theming.html instead

export const COLORS = {
  // DARK: '#1e1e1e',
  DARK: '#000',
  LIGHT: '#8a8c8b',
  TEXT: '#1e1e1e',
};