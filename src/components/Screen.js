import React from 'react';
import styled from 'styled-components';
import { COLORS } from "../constants/theme";

const Screen = styled.div`
    a {
      color: ${COLORS.DARK}
    }
    ${({full}) => full ? 'min-height: 100vh;' : ''}
    ${({centeredContent}) => centeredContent ? 'text-align: center;' : ''}
  `;

export default ({ className, ...other }) => {
  return (
    <Screen className={`d-flex flex-column justify-content-around ${className}`} {...other} />
  )
};
