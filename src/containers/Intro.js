import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components'

import Screen from '../components/Screen';
import ZebraTitle from '../components/ZebraTitle';
import Button from '../components/Button';

import { ROUTES } from '../constants/routes';
import { COLORS } from '../constants/theme';

import img from '../images/intro.png';

export default () => {
  const Intro = styled(Screen)`
    a {
      color: ${COLORS.DARK};
    }
    
    p {
        color: ${COLORS.LIGHT};
    }
    
    img {
      max-width: 50%;
      align-self: center;
    }
  `;

  return (
    <Intro full>
      <ZebraTitle phrase='Hookup __ with somebody __ awesome __ now!'/>

      <img src={img} alt="Find somebody"/>

      <div>
        <p>You agree to <a href='https://pure.dating/terms' target="_blank">Terms and Conditions</a> by pressing this button</p>

        <Link to={`/${ROUTES.ENTER_PHONE}`}>
          <Button>I'm down</Button>
        </Link>
      </div>
    </Intro>
  )
}
