import React, { Component } from 'react';

import Screen from '../components/Screen';
import ZebraTitle from '../components/ZebraTitle';
import Input from '../components/Input';
import { Link } from "react-router-dom";
import { ROUTES } from "../constants/routes";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import soul from "../services/soul";
import { reduxForm } from "redux-form";
import styled from "styled-components";
import { COLORS } from "../constants/theme";

const StyledScreen = styled(Screen)`
  a {
    color: ${COLORS.DARK};
    margin-top: 12px;
    display: inline-block;
  }
`;

class VerifyPhone extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: ''
    }
  }

  checkPhone(e, code) {
    this.setState({
      code,
      error: null
    });
    if (code.length === 5) {
      //Fixme Fake code verification
      if (code === '12345') {
        soul.auth.phone.verify({code})
          .then(res => {
            console.log(res);
            this.props.phoneVerified();

            //FIXME must be movet outside
            this.props.userAuthorized();
            //TODO find a better place for redirect
            this.props.dispatch(push(ROUTES.PERMISSIONS))
          });
      } else {
        setTimeout(() => {
          this.setState({
            error: 'Wrong code'
          })
        }, 250)
      }
    }
  }

  render() {
    const max = (v) => (v.length > 5 ? v.slice(0, 5) : v)
    const { phone } = this.props;
    const { countryCode } = phone;

    return (
      <StyledScreen full>
        <ZebraTitle phrase="Enter__ the code we sent __you" />
        <div>
          <p>You should've received an SMS with the code at {countryCode.code}{phone.number} </p>
          <Input normalize={max} invalid={this.state.error} name='veryficationCode' component="input" type="number"  placeholder='12345' onChange={this.checkPhone.bind(this)}/>
          {/*<p>I didn't receive a code (:20)</p>*/}
          <p><Link to={ROUTES.ENTER_PHONE}> Edit phone number</Link></p>
        </div>
        <div></div>
      </StyledScreen>
    )
  }
}

VerifyPhone = reduxForm({
  form: 'verifyPhone'
})(VerifyPhone);

VerifyPhone = connect(
  ({phone}) => ({phone}),
  (dispatch) => ({
    phoneVerified() {
      dispatch({
        type: 'PHONE_VERIFIED'
      })
    },
    userAuthorized() {
      dispatch({
        type: 'USER_AUTH',
        isAuthenticated: true
      })
    }
  })
)(VerifyPhone);

export default VerifyPhone;