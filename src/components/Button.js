import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../constants/theme';

const Button = styled.button`
    background: ${COLORS.DARK};
    color: white;
    padding: 20px 100px;
    font-size: 18px;
    text-transform: uppercase;
    outline: none;
    ${({disabled}) => (disabled ? 'opacity: 0.5' : 'cursor: pointer;')}
  `;

export default ({...rest}) => (
  <Button {...rest} />
)
