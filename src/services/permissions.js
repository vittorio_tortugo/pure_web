export const permission = {
  requestNotification() {
    return Notification.requestPermission()
  },

  requestLocation() {
    // return navigator.permissions.query({name:'geolocation'});
  },

  hasNotificationPermission() {
    return Notification.permission === 'granted';
  }
};