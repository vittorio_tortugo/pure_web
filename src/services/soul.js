import SoulSDK from 'soul-sdk';
import config from '../config.json';

export default new SoulSDK({
  apiKey: config.apiKey,
  app: {
    name: 'Pure',
    version: '1.0'
  },
  chats: {
    subscribeKey: config.chats.subscribeKey,
    publishKey: config.chats.publishKey,
  },
  apiEndpoint: config.apiEndpoint
});
