import React, { Component } from 'react';

import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider, connect } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux'

import soul from './services/soul';
import { Route, Switch } from 'react-router-dom';
import { reducer as formReducer } from 'redux-form';

import { userReducer, permissionReducer, phoneReducer } from './reducers';

import Intro from './containers/Intro';
import SendRequest from './containers/SendRequest';
import EnterPhone from './containers/EnterPhone';
import VerifyPhone from './containers/VerifyPhone';

import { Container, Row, Col } from 'reactstrap'


import { ROUTES } from './constants/routes';
import Permissions from "./containers/Permissions";
import { loadState, saveState } from "./services/localStorage";
import throttle from "lodash/throttle";

const history = createHistory();
const middleware = routerMiddleware(history);

const persistentStore = loadState();
const store = createStore(
  combineReducers({
    user: userReducer,
    permissions: permissionReducer,
    phone: phoneReducer,
    router: routerReducer,
    form: formReducer
  }),
  persistentStore,
  applyMiddleware(middleware),
);

store.subscribe(throttle(() => {
  saveState(store.getState());
}, 1000));

export const Root = () => (
  <Provider store={store}>
    <App/>
  </Provider>
);

class App extends Component {
  constructor(props) {
    super(props);

    this.props.auth()
      .then(res => {
        this.setState({loading: false})
      })
  }

  state = {
    loading: true
  };

  render() {
    return (
      <ConnectedRouter history={history}>
        <Container>
          <Row>
            <Col xs={{size: 10, offset: 1}} sm={{size: 8, offset: 2}} md={{size: 6, offset: 3}}
                 lg={{size: 4, offset: 4}}>
              {!this.state.loading &&
              <Switch>
                <Route path={`/${ROUTES.ENTER_PHONE}`} component={EnterPhone}/>
                <Route path={`/${ROUTES.VERIFY_PHONE}`} component={VerifyPhone}/>
                <Route path={`/${ROUTES.PERMISSIONS}`} component={Permissions}/>
                <Route path='/' component={this.props.user.isAuthenticated ? SendRequest : Intro} exact/>
              </Switch>
              ||
              <p>Loading...</p>
              }
            </Col>
          </Row>
        </Container>
      </ConnectedRouter>
    );
  }
}

App = connect(
  ({user}) => ( {user} ),
  dispatch => ( {
    auth() {
      return soul.auth.isAuthenticated()
        .then(user => {
          dispatch({
            type: 'USER_AUTH',
            isAuthenticated: true
          })
        })
        .catch(err => {
          dispatch({
            type: 'USER_AUTH',
            isAuthenticated: false
          })
        })
    }
  } )
)(App);