export const readFile = (e) => {
  return new Promise(res => {
    const reader = new FileReader();
    const file = e.target.files[0];

    console.log(file);

    reader.onloaded = () => {
      console.log('loaded')
      res(file, reader);
    };

    reader.readAsDataURL(file)
  });
};