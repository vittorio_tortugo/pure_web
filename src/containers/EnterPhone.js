import React, { Component } from 'react';
import { connect } from "react-redux";
import styled from 'styled-components'
import { callingCountries } from 'country-data';
import flatten from 'lodash/flatten';
import orderBy from 'lodash/orderBy';

import Screen from '../components/Screen';
import ZebraTitle from '../components/ZebraTitle';
import Input from '../components/Input';
import Button from '../components/Button';
import Select from '../components/Select';
import { push } from "react-router-redux";
import { ROUTES } from "../constants/routes";
import soul from "../services/soul";
import { reduxForm } from "redux-form";
import ErrorMessage from "../components/ErrorMessage";

const EnterScreen = styled(Screen)`
  .form-group * {
    width: 100%;
  }
`;

//TODO redirect to index page if props.phone.isVerified
//TODO move actions to separated file
class EnterPhone extends Component {
  constructor(props) {
    super(props);

    let countries = flatten(callingCountries.all.map(country => {
       return country.countryCallingCodes.map(code => {
         return {
           label: `${country.name} (${code.replace(' ', '')})`,
           code
         }
       });
    }));

    countries = orderBy(countries, [c => c.label]);
    countries = countries.map((country, index) => {
      country.index = index;
      return country;
    });

    this.state = {
      countries
    }
  }

  onCountryCodeSelected(event, countryIndex) {
    this.props.onCountryCodeChanged(this.state.countries[countryIndex]);
  }

  onNextButtonClicked() {
    const { number, countryCode } = this.props.phone;

    soul.auth.phone.getCode({phoneNumber: `${countryCode.code}${number}`.replace(' ', '')})
      .then(res => {
        this.props.proceed();
      })
      .catch(res => {
        this.setState({
          error: 'Something went wrong, please try again later'
        })
      })
  }

  render() {
    const { countries } = this.state;
    const { onNumberChanged, phone } = this.props;

    return (
      <EnterScreen full>
        <ZebraTitle phrase="Give us __ your phone __ number" inverted/>

        {this.state.error && <ErrorMessage>{this.state.error}</ErrorMessage>}

        <div className='form-group'>
          <p>We hate bots, they upset awesome people.<br />Need to make sure you're not one.</p>
          <Input placeholder='Enter your phone' name='number' component="input" type="number" onChange={onNumberChanged.bind(this)} />
          <Select name='countries' component='select' onChange={this.onCountryCodeSelected.bind(this)}>
            <option>Select your country code</option>
            {countries.map((option, i) => <option key={i} value={i}>{option.label}</option>)}
          </Select>
        </div>

        <Button disabled={!phone.number || !phone.countryCode} onClick={this.onNextButtonClicked.bind(this)}>Next</Button>
      </EnterScreen>
    )
  }
}

EnterPhone = reduxForm({
  form: 'enterPhone'
})(EnterPhone);

EnterPhone = connect(
  ({user, phone}) => {
    const {number, countryCode = {}} = phone;

    return {
      user,
      phone,
      initialValues: {
        number: number,
        countries: countryCode.index
      }
    }
  },
  (dispatch) => ({
    onNumberChanged(e, number) {
      dispatch({
        type: 'CHANGED_NUMBER',
        number
      })
    },

    onCountryCodeChanged(countryCode) {
      dispatch({
        type: 'CHANGED_COUNTRY_CODE',
        countryCode
      })
    },

    proceed() {
      dispatch(push(ROUTES.VERIFY_PHONE))
    }
  })
)(EnterPhone);

export default EnterPhone;