const stateLSKey = 'pure_state';

export const loadState = () => {
  try {
    const stringifiedState = window.localStorage.getItem(stateLSKey);

    return  stringifiedState ? JSON.parse(stringifiedState) : undefined;
  } catch (e) {
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const stringifiedState = JSON.stringify(state);
    localStorage.setItem(stateLSKey, stringifiedState);
  }
  catch (e) {}
};